function defaults(obj, defaultProps) {
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    for (let key in defaultProps) {
        if (obj[key] === undefined) {
          obj[key] = defaultProps[key];
        }
      }
      return obj;
    
    // Return `obj`
}

module.exports = defaults