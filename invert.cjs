function invert(obj) {
    // Returns a copy of the object where the keys have become the values and the values the keys.
    let invertObject ={};
    for (let key in obj){
        invertObject[obj[key]] =key
    }

    return invertObject
    // Assume that all of the object's values will be unique and string serializable.
}

module.exports = invert