
function keys(obj) {
    // Retrieve all the names of the object's properties.
    let keysObj =[];
    for (let key in obj){
        keysObj.push(key)

    }
    // Return the keys as strings in an array.
    return keysObj
}

module.exports=keys