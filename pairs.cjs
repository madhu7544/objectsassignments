function pairs(obj) {
    // Convert an object into a list of [key, value] pairs.
    let keyValues =[];
    for (let val in obj){
        keyValues.push([val,obj[val]])
    }
    return keyValues
}

module.exports = pairs
